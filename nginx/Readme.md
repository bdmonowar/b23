# Nginx Deployment on Kubernetes

- After deployment of app1 and app2, Check their nodeports

```
kubectl get svc

```
- Then replace the app1 nodeport and app2 nodeport in the configmap.yaml

- Next Run the yamls

```
kubectl apply -f 1.configMap.yaml
kubectl apply -f 2.nginx-deploy.yaml

```
- Access app1 and app2 from your browser

```
http://app.k8slabs.bd/app1
http://app.k8slabs.bd/app2

```
