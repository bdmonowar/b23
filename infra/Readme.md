# Kubernetes Cluster Installation for Master

## System config

```
hostnamectl set-hostname k8smaster.k8slabs.bd
cat >> /etc/hosts  << EOF

127.0.0.1       k8smaster.k8slabs.bd k8smaster
192.168.7.123   k8smaster.k8slabs.bd k8smaster
192.168.7.124   k8sw01.k8slabs.bd k8smaster
EOF
sed -i --follow-symlinks 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux
swapoff -a
sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab
systemctl disable firewalld && systemctl stop firewalld
lsmod | grep br_netfilter
firewall-cmd --add-port={6443,2379-2380,10250,10251,10252,5473,179,5473}/tcp --permanent
firewall-cmd --add-port={4789,8285,8472}/udp --permanent
firewall-cmd --reload
modprobe br_netfilter
lsmod | grep br_netfilter
echo '1' > /proc/sys/net/bridge/bridge-nf-call-iptables
cat >> /etc/sysctl.d/k8s.conf << EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sysctl --system
reboot
```

## Install Docker

```
yum -y update && yum install -y vim wget net-tools yum-utils device-mapper-persistent-data lvm2
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum -y install docker-ce docker-ce-cli containerd.io
sed -i 's/disabled_plugins/#disabled_plugins/g' /etc/containerd/config.toml
#containerd config default > /etc/containerd/config.toml
#echo '' >> /etc/containerd/config.toml
systemctl enable docker containerd && systemctl start docker containerd && systemctl status docker containerd

```

## Kubectl Install

```
cat > /etc/yum.repos.d/kubernetes.repo << EOF
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg
       https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
yum -y update && yum install -y kubelet kubeadm kubectl kubernetes-cni
docker info | grep -i cgroup
cat >> /usr/lib/systemd/system/kubelet.service.d/10-kubeadm.conf << EOF
cgroup-driver=cgroupfs
EOF

mkdir -p $HOME/.kube

kubeadm init --apiserver-advertise-address=192.168.7.123 --pod-network-cidr=192.168.0.0/16
cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
chown $(id -u):$(id -g) $HOME/.kube/config
systemctl start kubelet && sleep 5 && systemctl status kubelet
systemctl enable kubelet && systemctl start kubelet && sleep 5 && systemctl status kubelet

kubectl create -f https://docs.projectcalico.org/manifests/tigera-operator.yaml
kubectl create -f https://docs.projectcalico.org/manifests/custom-resources.yaml

kubectl get nodes
kubectl config view

kubectl logs -n kube-system <pod name>

kubeadm token create --print-join-command

```

## Check the kubernetes cluster nodes

```
kubectl get nodes -o wide && kubectl get pods --all-namespaces -o wide

```

# Kubernetes Installation for 2 Worker

## System config

```
yum -y update && yum install -y vim wget net-tools yum-utils device-mapper-persistent-data lvm2
hostnamectl set-hostname k8sworker.k8slabs.bd
cat > /etc/hosts << EOF

127.0.0.1       k8sworker.k8slabs.bd k8sworker
192.168.7.124   k8sworker.k8slabs.bd k8sworker
EOF
sed -i --follow-symlinks 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux
swapoff -a
sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab
systemctl disable firewalld && systemctl stop firewalld
firewall-cmd --permanent --add-port=10250/tcp
firewall-cmd --permanent --add-port=10255/tcp
firewall-cmd --permanent --add-port=30000-32767/tcp
firewall-cmd --permanent --add-port=6783/tcp
firewall-cmd  --reload

modprobe br_netfilter
lsmod | grep br_netfilter
echo '1' > /proc/sys/net/bridge/bridge-nf-call-iptables
cat > /etc/sysctl.d/k8s.conf << EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sysctl --system
reboot

## Install Docker

```
yum -y update && yum install -y vim wget net-tools yum-utils device-mapper-persistent-data lvm2
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum -y install docker-ce docker-ce-cli containerd.io
containerd config default > /etc/containerd/config.toml
systemctl enable docker containerd && systemctl start docker containerd && systemctl status docker containerd

```

## Kubectl Install

```
cat > /etc/yum.repos.d/kubernetes.repo <<EOF
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg
       https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
yum check-update && yum install kubeadm -y
systemctl restart docker containerd && systemctl status docker containerd
kubeadm token create --print-join-command # exectue this command first master node

systemctl enable kubelet && systemctl start kubelet && sleep 5 && systemctl status kubelet
systemctl restart docker containerd && systemctl status docker containerd

# after ready then
reboot

kubeadm join 192.168.7.123:6443 --token qc4lw9.84v4qu442dhra3cn --discovery-token-ca-cert-hash sha256:20df92f43d56216768d4a3e222257836182696dc153c85549c29d88c51bfb4cf

```

## Jenkins Install and config

```
curl -fsSL https://pkg.jenkins.io/debian-stable/jenkins.io-2023.key | sudo tee /usr/share/keyrings/jenkins-keyring.asc > /dev/null

echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] https://pkg.jenkins.io/debian-stable binary/ | sudo tee /etc/apt/sources.list.d/jenkins.list > /dev/null

apt install curl debian-archive-keyring lsb-release ca-certificates apt-transport-https software-properties-common -y
apt install -y default-jre default-jdk
apt-get -y update && apt-get -y upgrade
apt-get -y install jenkins
systemctl start jenkins && systemctl enable jenkins && systemctl status jenkins

cat /var/lib/jenkins/secrets/initialAdminPassword

## Nginx Install and config

upstream jenkins {
        ip_hash;
        server 127.0.0.1:8080;
}

server {
    if ($host = jenkins.k8slabs.bd) {
        return 301 https://$host$request_uri;
    }
    listen 80;
    server_name jenkins.k8slabs.bd ;
    return 404;
}

server {
    listen 443 ssl http2;
    server_name jenkins.k8slabs.bd;
    ssl_certificate /etc/letsencrypt/live/jenkins.k8slabs.bd/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/jenkins.k8slabs.bd/privkey.pem;
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
    ssl_session_timeout 60m;
    ssl_prefer_server_ciphers on;
    ssl_session_cache shared:le_nginx_SSL:10m;
    ssl_session_tickets off;
    ssl_protocols TLSv1.2 TLSv1.3;
    ssl_ciphers "ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384";
    add_header Strict-Transport-Security "max-age=31536000";

    location / {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_pass http://jenkins;
    }
}
```
