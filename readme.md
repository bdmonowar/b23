# Devops-Jenkins-CiCd-Kubernetes
This is a devops task that contains laravel app with jenkins CI/CD, Kubernetes Deployments with Nginx,Jenkins Docker container setup with docker compose amd Lightweight cluster setup

Table of contents
--
- [Lightweight Kubernetes Cluster Installation](https://gitlab.com/bdmonowar/b23/-/blob/main/infra/Readme.md?ref_type=heads)

- [Jenkins Container Setup Using Docker Compose](https://github.com/shawon100/devops-task-laravel-cicd-nginx-kubernetes/blob/main/jenkins-container/Readme.md)

- [Jenkins Pipeline Setup for App1](https://gitlab.com/bdmonowar/b23/-/blob/main/app1/README.md?ref_type=heads)

- [Jenkins Pipeline Setup for App2](https://gitlab.com/bdmonowar/b23/-/blob/main/app2/README.md?ref_type=heads)

- [Nginx Setup on Kubernetes](https://github.com/shawon100/devops-task-laravel-cicd-nginx-kubernetes/blob/main/nginx/Readme.md)